# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond import backend
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pool import Pool
from trytond.pyson import Eval, Id
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration(
    ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Staff Configuration'
    __name__ = 'staff.configuration'
    staff_contract_sequence = fields.Many2One('ir.sequence', 'Contract Sequence',
        required=True, domain=[('sequence_type', '=',
                                Id('staff', 'sequence_type_staff_contract')), ])

    default_staff_contract_sequence = default_func('staff_contract_sequence')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'staff_contract_sequence':
            return pool.get('staff.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)


class StaffConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Staff Configuration Sequence"
    __name__ = 'staff.configuration.sequence'
    staff_contract_sequence = fields.Many2One(
        'ir.sequence', "Staff Contract Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=', Id('staff', 'sequence_type_staff_contract'))
            ],
        depends=['company'])

    @classmethod
    def __register__(cls, module_name):
        exist = backend.TableHandler.table_exist(cls._table)
        super(StaffConfigurationSequence, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('staff_contract_sequence')
        value_names.append('staff_contract_sequence')
        fields.append('company')
        migrate_property(
            'staff.configuration', field_names, cls, value_names,
            fields=fields)

    @classmethod
    def default_staff_contract_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('staff', 'sequence_staff_contract')
        except KeyError:
            return None
