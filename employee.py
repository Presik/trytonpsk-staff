# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, Unique
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    active = fields.Boolean('Active')
    code = fields.Char('Code', size=None, select=True)
    salary = fields.Function(fields.Numeric('Salary', digits=(16, 2),
        help='Salary monthly of person',
        depends=['contract'], readonly=True), 'get_salary')
    contract = fields.Function(fields.Many2One('staff.contract',
        'Contract'), 'get_contract', searcher='search_contract')
    position = fields.Function(fields.Many2One('staff.position', 'Position'), 'get_position')
    curses = fields.Text('Curses')
    career = fields.Text('Career')
    contracting_state = fields.Selection([
            ('', ''),
            ('aspirant', 'Aspirant'),
            ('rejected', 'Rejected'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'Contracting State')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [(
            'party_uniq', Unique(table, table.party), 'Employee already exists!'
        )]

    @classmethod
    def __register__(cls, module_name):
        super(Employee, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        # Migration: Rename in_process to aspirant into contracting_state
        cursor.execute(*sql_table.update(
            columns=[sql_table.contracting_state],
            values=['aspirant'],
            where=(sql_table.contracting_state == 'in_process')
        ))

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    def get_position(self, name=None):
        if self.contract and self.contract.position:
            return self.contract.position.id

    def get_contract(self, name=None):
        Contract = Pool().get('staff.contract')
        contracts = Contract.search([
                ('employee', '=', self.id),
                ('state', '=', 'active'),
            ])

        if contracts:
            return contracts[0].id

    @classmethod
    def search_contract(cls, name, clause):
        return

    def get_salary(self, name=None):
        if self.contract:
            return self.contract.salary

    @fields.depends('contract')
    def on_change_with_salary(self):
        if self.contract:
            self.salary = self.contract.salary
